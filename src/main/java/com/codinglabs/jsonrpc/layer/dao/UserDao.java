package com.codinglabs.jsonrpc.layer.dao;

import java.io.Serializable;

import com.codinglabs.jsonrpc.abstracts.BasicOperations;

public interface UserDao<T extends Serializable> extends BasicOperations<T> {
	
}