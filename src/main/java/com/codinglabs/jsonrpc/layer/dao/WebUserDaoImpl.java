package com.codinglabs.jsonrpc.layer.dao;

import org.springframework.stereotype.Repository;

import com.codinglabs.jsonrpc.abstracts.AbstractHibernateDao;
import com.codinglabs.jsonrpc.dto.entity.WebUser;

@Repository
public class WebUserDaoImpl extends AbstractHibernateDao<WebUser> implements UserDao<WebUser> {

	public WebUserDaoImpl() {
		super();
		super.setKlazz(WebUser.class);
	}
	
}
