package com.codinglabs.jsonrpc.service;

import java.util.List;

import com.codinglabs.jsonrpc.dto.entity.WebUser;
import com.googlecode.jsonrpc4j.JsonRpcService;
import com.jsonrpc.pub.exceptions.AlreadyExistsException;
import com.jsonrpc.pub.exceptions.NotFoundException;

@JsonRpcService("/web-user-service")
public interface WebUserService {

	boolean createWebUser(WebUser user) throws AlreadyExistsException;
	boolean updateWebUser(WebUser user) throws NotFoundException;
	WebUser findOneById(long id) throws NotFoundException;
	WebUser findOneByUsername(String username) throws NotFoundException;
	List<WebUser> findAll() throws NotFoundException;

}