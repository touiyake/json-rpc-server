package com.codinglabs.jsonrpc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codinglabs.jsonrpc.dto.entity.WebUser;
import com.codinglabs.jsonrpc.layer.dao.UserDao;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import com.jsonrpc.pub.exceptions.AlreadyExistsException;
import com.jsonrpc.pub.exceptions.EmailAlreadyExistsException;
import com.jsonrpc.pub.exceptions.NotFoundException;
import com.jsonrpc.pub.exceptions.UserAlreadyExistsException;
import com.jsonrpc.pub.exceptions.UserNotFoundException;

@Service
@AutoJsonRpcServiceImpl
public class WebUserServiceImpl implements WebUserService {

	@Autowired
	private UserDao<WebUser> dao;
	
	@Override
	public boolean createWebUser(WebUser user) throws AlreadyExistsException {
		WebUser tempUser = this.dao.findOneByKeyVal("username", user.getUsername());
		if (tempUser != null) {
			throw new UserAlreadyExistsException("Username already exists");
		}
		
		tempUser = this.dao.findOneByKeyVal("email", user.getEmail());
		if (tempUser != null) {
			throw new EmailAlreadyExistsException("Email Address already exists");
		}
		
		boolean created = this.dao.create(user);
		if (created) return true;
		return false;
	}

	@Override
	public boolean updateWebUser(WebUser user) throws NotFoundException {
		WebUser tempUser = this.dao.findOneByKeyVal("username", user.getUsername());
		if (tempUser == null) {
			throw new UserNotFoundException("Username not exists");
		}
		
		boolean updated = this.dao.update(user);
		if (updated) return true;
		return false;
	}

	@Override
	public WebUser findOneByUsername(String username) throws NotFoundException {
		WebUser user = this.dao.findOneByKeyVal("username", username);
		if (user != null) {
			return user;
		} else {
			throw new UserNotFoundException("Username not exists");
		}
	}

	@Override
	public WebUser findOneById(long id) throws NotFoundException {
		WebUser user = this.dao.findOneById(id);
		if (user != null) {
			return user;
		} else {
			throw new UserNotFoundException("User ID not exists");
		}
	}

	@Override
	public List<WebUser> findAll() throws NotFoundException {
		List<WebUser> list = this.dao.findAll();
		if (list.size() > 0) {
			return list;
		} else {
			throw new NotFoundException("No record(s) found");
		}
	}

}