package com.codinglabs.jsonrpc.dto.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Embeddable
public class Address implements Serializable {

	private String city;
	private String state;
	private String country;

}