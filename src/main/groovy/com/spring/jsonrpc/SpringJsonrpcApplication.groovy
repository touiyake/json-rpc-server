package com.spring.jsonrpc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SpringJsonrpcApplication {

	static void main(String[] args) {
		SpringApplication.run(SpringJsonrpcApplication, args)
	}

}
